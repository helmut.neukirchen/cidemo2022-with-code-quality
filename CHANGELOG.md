## 1.3.0 (2022-03-15)

### change (2 changes)

- [#8 updated readme](helmut.neukirchen/cidemo2022-with-code-quality@fa4612c29e4f1770e7272efad2378523d5b8d607)
- [#7 updated readme](helmut.neukirchen/cidemo2022-with-code-quality@83399c9671e8261887a9e6ff466cb703bd788c58)

### fixed (1 change)

- [#6 Fixed bug](helmut.neukirchen/cidemo2022-with-code-quality@c0bbe855613aa556666e3f83ab2a0e353abb4089)

## 1.2.0 (2022-03-15)

### changed (1 change)

- [#3 Improved feature](helmut.neukirchen/cidemo2022-with-code-quality@28ff6e57d0d749d9305e818c482cfdb326b6c2f7)

### fixed (1 change)

- [#4 Fixed bug](helmut.neukirchen/cidemo2022-with-code-quality@28a36ed4ce64ab49745e6093ebc1f93d9649c55d)

### other (1 change)

- [#5 Updated existing changelog change](helmut.neukirchen/cidemo2022-with-code-quality@07774a0d285b53d556e5bce968f0beef6cc713ed)

## 1.1.0 (2022-03-15)

### other (1 change)

- [#2 added some other infrastructure change](helmut.neukirchen/cidemo2022-with-code-quality@8812679337e6637bbe3c8b2618807b814073407b)

### fixed (1 change)

- [#2 added infrastructure](helmut.neukirchen/cidemo2022-with-code-quality@008194630c2a4a391750ba7f8daec026472ba661)

### added (1 change)

- [#2 added feature](helmut.neukirchen/cidemo2022-with-code-quality@78757f0b3bcd59342421c1c7aae99ad21d0a5c98)
